package co.mtarget.crud.controller

import co.mtarget.crud.dto.DetailData
import co.mtarget.crud.model.Crud
import co.mtarget.crud.service.DataService
import co.mtarget.vesko.web.*
import org.koin.core.KoinComponent
import org.koin.core.inject

@Controller("/", tags = ["Crud"])
class CrudController : KoinComponent {

    private val dataservice: DataService by inject()

    @GET("/ping")
    fun ping(): String {
        return "up"
    }

    @GET("/data")
    suspend fun data(): DetailData {
        return dataservice.dapatData()
    }

    @GET("/alldata")
    suspend fun alldata(): List<Crud>{
        return dataservice.dapatAllData()
    }

    @POST("/insert")
    suspend fun insert(@BodyParam form: DetailData): DetailData {
        return dataservice.tambahData(
            form.nama,
            form.jk,
            form.umur
        )
    }

    @DELETE("/delete")
    suspend fun delete(@BodyParam params: FormDelete): Crud {
        return dataservice.hapusData(params.id)
    }

    @PUT("/update")
    suspend fun update(@BodyParam params: FormUpdate): Crud {
        return dataservice.updateData(params.id, params.nama, params.umur)
    }


}

class FormUpdate {
    var id: String = ""
    var nama: String = ""
    var umur: Int = 0
}

class FormDelete {
    var id: String = ""
}

