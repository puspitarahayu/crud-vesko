package co.mtarget.crud.service

import co.mtarget.crud.dto.DetailData
import co.mtarget.crud.model.Crud
import org.bson.types.ObjectId
import co.mtarget.crud.NamaAlreadyRegistered
import org.mongodb.morphia.AdvancedDatastore
import co.mtarget.crud.DataKosong
import co.mtarget.kohmon.getStackTraceAsString
import co.mtarget.vesko.vertxLogger
import io.vertx.core.json.Json
import io.vertx.core.json.JsonObject
import org.koin.core.KoinComponent
import org.koin.core.inject

class DataService : KoinComponent
{
    private val datastore: AdvancedDatastore by inject()

    private val log = vertxLogger(this::class)

    fun dapatData(): DetailData {
        val da = datastore.find(Crud::class.java).first()
        return DetailData(da)
    }

    fun tambahData(nama: String, jk: String, umur: Int?): DetailData {
        if (checkNama(nama)) throw NamaAlreadyRegistered()
        val user = Crud()
        user.nama = nama
        user.jk = jk
        user.umur = umur
        datastore.save(user)
        return DetailData(user)
    }

    private fun checkNama(nama: String): Boolean {
        return datastore.find(Crud::class.java).field("nama").equal(nama).get() != null
    }

    fun hapusData(id: String): Crud {
        val del = datastore.get(Crud::class.java, ObjectId(id))
        if (del == null) throw DataKosong()
        datastore.delete(del)
        return Crud()
    }

    fun updateData(id: String, nama: String, umur: Int?): Crud {
        val data = Crud()
        data.nama = nama
        data.umur = umur

        val query = datastore.get(Crud::class.java, ObjectId(id))
        val opsAdd = datastore.createUpdateOperations(Crud::class.java).set("nama",nama)
            .set("umur",umur)
        datastore.update(query, opsAdd)
        return Crud()
    }

    fun dapatAllData(): List<Crud> {
        val data = datastore.find(Crud::class.java).asList()
        return data
    }


}


