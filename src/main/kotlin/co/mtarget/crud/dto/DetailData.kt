package co.mtarget.crud.dto

import co.mtarget.crud.model.Alamat
import co.mtarget.crud.model.Crud
import co.mtarget.crud.model.Pendidikan



data class DetailData constructor(
    val nama: String,
    val jk: String,
    val umur: Int?,
    val alamat: ArrayList<Alamat> = arrayListOf(),
    val pendidikan: ArrayList<Pendidikan> = arrayListOf()
){
    constructor(crud: Crud) : this(
        //init
        crud.nama,
        crud.jk,
        crud.umur,
        crud.alamat,
        crud.pendidikan
    )
}
