package co.mtarget.crud

import kotlin.system.exitProcess
import co.mtarget.vesko.Vesko
import co.mtarget.vesko.vertxLogger
import kotlinx.coroutines.runBlocking

fun main(): Unit = runBlocking {

    val vesko = Vesko.Builder.create()
    val log = vertxLogger(this::class)
    log.info("vertx crud app started")
    try {
        vesko.deployMicroService(ApiVerticle())
    } catch (e: Exception) {
        e.printStackTrace()
        vesko.close()
        exitProcess(1)
    }
}