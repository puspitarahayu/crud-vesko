package co.mtarget.crud.model

import org.bson.types.ObjectId
import org.mongodb.morphia.annotations.Entity
import org.mongodb.morphia.annotations.Id


@Entity("data")
class Crud {

    @Id
    var id = ObjectId()
    var nama: String = ""
    var jk: String = ""
    var umur: Int? = 0
    var alamat: ArrayList<Alamat> = arrayListOf()
    var pendidikan: ArrayList<Pendidikan> = arrayListOf()

}

class Pendidikan() {
    var jenjang: String = ""
    var namaSekolah: String = ""
    var tahunLulus: Int = 0
}

class Alamat() {
    var jalan: String = ""
    var kota: String = ""
    var provinsi: String = ""
}
