package co.mtarget.crud.config

import io.vertx.core.Verticle
import io.vertx.core.Vertx
import io.vertx.core.file.FileSystem
import io.vertx.core.spi.VerticleFactory
import org.koin.core.KoinApplication
import org.koin.core.KoinComponent
import org.koin.core.context.startKoin
import org.koin.core.get
import org.koin.core.module.Module
import org.koin.core.qualifier.named
import org.koin.dsl.module

fun enableDependenyInjection(modules: KoinApplication.() -> Unit) {
    startKoin {
        modules()
    }
}

fun KoinApplication.load(vararg elements: Module) = modules(elements.asList())

object KoinVerticleFactory : VerticleFactory, KoinComponent {
    override fun prefix(): String = "koin"

    override fun createVerticle(verticleName: String, classLoader: ClassLoader): Verticle {
        return get(named(verticleName.substringAfter("koin:")))
    }
}

fun enableFileSystem(vertx: Vertx) = module {
    single { fileSystem(vertx) }
}

fun fileSystem(vertx: Vertx): FileSystem {
    return vertx.fileSystem()
}
