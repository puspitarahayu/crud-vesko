package co.mtarget.crud.config

import co.mtarget.vesko.AuthException
import co.mtarget.vesko.auth.AuthHeader
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import io.vertx.ext.auth.PubSecKeyOptions
import io.vertx.ext.auth.jwt.JWTAuth
import io.vertx.ext.auth.jwt.JWTAuthOptions
import io.vertx.kotlin.ext.auth.authenticateAwait
import org.koin.dsl.module

fun enableJwt(vertx: Vertx, config: JsonObject) = module {
    single { jwtAuth(vertx, config) }
}

fun jwtAuth(vertx: Vertx, config: JsonObject): CachedJwtAuth {
    val redis = redis(vertx, config)
    val redisCache = RedisCache(redis)
    val jwtConfig = jwtConfig(config)
    val jwt = JWTAuth.create(vertx, jwtConfig)
    val jwtConf = config.getJsonObject("jwt", JsonObject())
    val rateLimiter = RedisRateLimiter(
        redis,
        jwtConf.getInteger("rateLimit", 200),
        jwtConf.getInteger("rateLimitReset", 300)
    )
    val tokenReset = jwtConf.getInteger("tokenReset", 1800)
    return CachedJwtAuth(jwt, redisCache, rateLimiter, tokenReset)
}

fun AuthHeader.payload(): JsonObject = JsonObject().put("jwt", this.token)

fun JWTAuthOptions.useSimetricKey(pubKey: String) = this.apply {
    pubSecKeys = listOf(
        PubSecKeyOptions().apply {
            algorithm = "HS256"
            publicKey = pubKey
            isSymmetric = true
        }
    )
}

fun jwtConfig(config: JsonObject): JWTAuthOptions {
    val jwtConfig = config.getJsonObject("jwt") ?: JsonObject()
    val symmetricKey = jwtConfig.getString("symmetricKey") ?: "MTARGET"
    return JWTAuthOptions().useSimetricKey(symmetricKey)
}

class CachedJwtAuth(
    private val jwt: JWTAuth,
    private val cache: AuthCache,
    private val rateLimiter: RateLimiter? = null,
    private val tokenReset: Int
) {

    suspend fun generateToken(json: JsonObject): String {
        val token = jwt.generateToken(json)
        cache.append(token, tokenReset)
        rateLimiter?.validate(token)
        return token
    }

    suspend fun authenticate(authHeader: AuthHeader): JsonObject {
        if(!cache.validate(authHeader.token)) throw AuthException(34, "invalid or expired token")
        rateLimiter?.validate(authHeader.token)
        return jwt.authenticateAwait(authHeader.payload()).principal()
    }

    suspend fun invalidate(authHeader: AuthHeader) {
        cache.invalidate(authHeader.token)
    }
}
