package co.mtarget.crud.config

import co.mtarget.vesko.vertxLogger
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.mongodb.*
import com.mongodb.client.MongoDatabase
import io.vertx.core.json.Json
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import org.bson.types.ObjectId
import org.koin.dsl.module
import org.mongodb.morphia.AdvancedDatastore
import org.mongodb.morphia.Datastore
import org.mongodb.morphia.Morphia
import org.mongodb.morphia.aggregation.AggregationPipeline
import org.mongodb.morphia.converters.TypeConverter
import org.mongodb.morphia.mapping.MappedField
import org.mongodb.morphia.mapping.Mapper
import org.mongodb.morphia.query.FindOptions
import org.mongodb.morphia.query.Query
import java.util.*
import java.util.regex.Pattern
import kotlin.reflect.KMutableProperty

fun enableMongodb(config: JsonObject) = module {
    single { datastore(config) }
    single { advanceDataStore(get()) }
    single { provideMongoDatabase(config) }
}

fun mongoClientShardingOptions(): MongoClientOptions = MongoClientOptions.builder()
    .connectTimeout(30000)
    .socketTimeout(60000)
    .connectionsPerHost(40)
    .readPreference(ReadPreference.secondaryPreferred())
    .readConcern(ReadConcern.MAJORITY)
    .writeConcern(WriteConcern.MAJORITY)
    .heartbeatConnectTimeout(120000)
    .maxConnectionIdleTime(60000)
    .maxConnectionLifeTime(120000)
    .threadsAllowedToBlockForConnectionMultiplier(1500)
    .build()

fun mongoClientSingleOptions(): MongoClientOptions = MongoClientOptions.builder()
    .connectionsPerHost(500)
    .threadsAllowedToBlockForConnectionMultiplier(50)
    .build()

fun morphia(): Morphia {
    val mapper = Mapper().apply {
        with(converters) {
            addConverter(JsonObjectConverter())
        }
    }
    return Morphia(mapper)
}

fun datastore(config: JsonObject): Datastore {
    val cfg = ConfigWrapper(
        try {
            config.getJsonObject("mongodb")
        } catch (e: ClassCastException) {
            config.getJsonArray("mongodb")
        }
    )
    val client = Mongodb(cfg).createClient()
    val dbname = cfg.getString("db") ?: throw IllegalArgumentException("dbname not found")
    return dataStore(client, dbname)
}

fun provideMongoDatabase(config: JsonObject): MongoDatabase {
    val cfg = ConfigWrapper(
        try {
            config.getJsonObject("mongodb")
        } catch (e: ClassCastException) {
            config.getJsonArray("mongodb")
        }
    )
    val mongoClient = Mongodb(cfg).createClient()
    val databaseName = cfg.getString("db")
    return mongoClient.getDatabase(databaseName)
}

fun dataStore(client: MongoClient, dbname: String): Datastore {
    return morphia().createDatastore(client, dbname).apply {
        ensureIndexes()
    }
}

fun advanceDataStore(datastore: Datastore): AdvancedDatastore {
    return (datastore as AdvancedDatastore)
}

class Mongodb(val cfg: ConfigWrapper) {
    private val log = vertxLogger(this::class)

    fun createClient(): MongoClient {
        val host = cfg.getString("host") ?: ""
        val port = cfg.getInteger("port") ?: 0
        val hosts = cfg.getJsonArray("hosts")
        val servers: ArrayList<ServerAddress> = arrayListOf()
        hosts?.forEach { sharding ->
            servers.add(ServerAddress(sharding as String, 27017))
        }

        val credential = MongoCredential.createCredential(
            cfg.getString("username"),
            cfg.getString("db"),
            cfg.getString("password")!!.toCharArray()
        )

        return if (!servers.isNullOrEmpty()) {
            shardedMongoClient(servers, credential)
        } else {
            val server = ServerAddress(host, port)
            singleMongoClient(server, credential)
        }
    }

    private fun shardedMongoClient(servers: List<ServerAddress>, credential: MongoCredential): MongoClient {
        val client = MongoClient(servers, arrayListOf(credential), mongoClientShardingOptions())
        val address = try {
            log.info("try connecting to sharding host ${servers.joinToString { it.host }}")
            client.allAddress.joinToString { it.host }
        } catch (e: Exception) {
            log.error("Mongo is not Connected [${e.message}]", e)
            client.close()
            throw e
        }
        log.info("MongoDB is Connected to $address")
        return client
    }

    private fun singleMongoClient(server: ServerAddress, credential: MongoCredential): MongoClient {
        val client = MongoClient(server, arrayListOf(credential), mongoClientSingleOptions())
        val address = try {
            log.info("Trying to Connect MongoDB Database [${server.host}:${server.port}]...")
            client.address
        } catch (e: Exception) {
            log.error("Mongo is not Connected [${e.message}]", e)
            client.close()
            throw e
        }
        log.info("MongoDB is Connected to $address")
        return client
    }
}

class JsonObjectConverter : TypeConverter(JsonObject::class.java) {
    override fun decode(targetClass: Class<*>, fromDBObject: Any, optionalExtraInfo: MappedField?): Any {
        return when (fromDBObject) {
            is Map<*, *> -> {
                val jsonEncode = Json.encode(fromDBObject)
                JsonObject(jsonEncode)
            }
            is String -> JsonObject(fromDBObject)
            else -> JsonObject()
        }
    }

    override fun encode(value: Any?, optionalExtraInfo: MappedField?): Any? {
        return if (value is JsonObject) {
            value.map
        } else {
            value
        }
    }
}

val defaultAggregationOptions: AggregationOptions = AggregationOptions.builder()
    .outputMode(AggregationOptions.OutputMode.CURSOR)
    .allowDiskUse(true)
    .bypassDocumentValidation(true)
    .build()

fun <T> Query<T>.searchRegexByField(text: String, fields: List<String>): Query<T> {
    if (text.isNotEmpty()) {
        val regexp = Pattern.compile(text)
        or(*fields.map { this.criteria(it).equal(regexp) }.toTypedArray())
    }
    return this
}

fun <T> Query<T>.date(dateField: String, start: Date? = null, end: Date? = null): Query<T> {
    if (start != null) this.field(dateField).greaterThanOrEq(start)
    if (end != null) this.field(dateField).lessThanOrEq(end)
    return this
}

fun <T> Query<T>.searchRegex(text: String, fields: List<KMutableProperty<*>>): Query<T> = searchRegexByField(text, fields.map { it.name })

fun <T> Query<T>.paginatedList(page: Int, size: Int): MutableList<T> = asList(FindOptions().pagination(page, size))


class ConfigWrapper(val cfg: Any) {
    fun getString(key: String): String? {
        return if (cfg is JsonObject) {
            cfg.getString(key)
        } else {
            (cfg as JsonArray).getJsonObject(0).getString(key)
        }
    }

    fun getInteger(key: String): Int? {
        return if (cfg is JsonObject) {
            cfg.getInteger(key)
        } else {
            (cfg as JsonArray).getJsonObject(0).getInteger(key)
        }
    }

    fun getJsonArray(key: String): JsonArray? {
        return if (cfg is JsonObject) {
            cfg.getJsonArray(key)
        } else {
            (cfg as JsonArray).getJsonObject(0).getJsonArray(key)
        }
    }

    fun findDbConfig(key: String): JsonObject? {
        return if (cfg is JsonObject) {
            null
        } else {
            (cfg as JsonArray).find {
                val obj = it as JsonObject
                obj.getString("db") == key
            } as JsonObject?
        }
    }
}

fun FindOptions.pagination(page: Int, size: Int): FindOptions {
    return this.skip((page - 1) * size).limit(size)
}

fun AggregationPipeline.pagination(page: Int, size: Int): AggregationPipeline {
    return this.skip((page - 1) * size).limit(size)
}

class ObjectIdDeserializer : JsonDeserializer<ObjectId>() {

    override fun deserialize(parser: JsonParser?, p1: DeserializationContext?): ObjectId {
        return ObjectId(parser?.valueAsString)
    }
}
