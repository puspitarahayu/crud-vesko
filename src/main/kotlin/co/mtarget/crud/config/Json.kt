package co.mtarget.crud.config

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule
import de.crunc.jackson.datatype.vertx.VertxJsonModule
import io.vertx.core.json.jackson.DatabindCodec
import org.koin.dsl.module

fun applyJsonModule() = module {
    single { provideObjectMapper() }
}

fun provideObjectMapper(): ObjectMapper {
    return DatabindCodec.mapper()
}

fun initJson() {

    DatabindCodec.mapper().apply {
        registerKotlinModule()
        registerModule(ParameterNamesModule())
        registerModule(JavaTimeModule())
        registerModule(VertxJsonModule())
        configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
    }

    DatabindCodec.prettyMapper().apply {
        registerKotlinModule()
        registerModule(ParameterNamesModule())
        registerModule(JavaTimeModule())
        configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
    }
}
