package co.mtarget.crud.config

import io.vertx.kotlin.redis.client.expireAwait
import io.vertx.kotlin.redis.client.getAwait
import io.vertx.kotlin.redis.client.setAwait
import io.vertx.redis.client.Redis

interface RateLimiter {
    suspend fun validate(token: String): Boolean
}

class RedisRateLimiter(private val redis: Redis, private val defaultLimit: Int, private val expiredInSecond: Int) :
    RateLimiter {

    override suspend fun validate(token: String): Boolean {
        val redisApi = redis.createApi()
        val response = redisApi.getAwait("$token.limit")
        return if (response == null) {
            redisApi.setAwait(listOf("$token.limit", "$defaultLimit"))
            redisApi.expireAwait("$token.limit", "$expiredInSecond")
            true
        } else {
            val limit = response.toInteger() - 1
            if (limit < 0) false
            else {
                redisApi.setAwait(listOf("$token.limit", "$limit"))
                true
            }
        }
    }
}
