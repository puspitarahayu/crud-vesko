package co.mtarget.crud.config

import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.redis.client.*
import io.vertx.redis.client.Redis
import io.vertx.redis.client.RedisAPI
import io.vertx.redis.client.RedisClientType
import io.vertx.redis.client.RedisRole

interface AuthCache {
    suspend fun append(token: String, expire: Int? = null, data: String = "")
    suspend fun getData(token: String): String
    suspend fun setData(token: String, data: String = ""): String
    suspend fun validate(token: String, expire: Int? = null): Boolean
    suspend fun invalidate(token: String): Boolean
}

fun redis(vertx: Vertx, config: JsonObject): Redis {
    val redisConf = config.getJsonObject("redis") ?: JsonObject()
    val redisOptions = io.vertx.redis.client.RedisOptions().apply {
        type = RedisClientType.STANDALONE
        val port = redisConf.getInteger("port", 6379)
        val host = redisConf.getString("host", "localhost")
        val password = redisConf.getString("password")
        val username = redisConf.getString("username", "developer")
        setConnectionString("redis://$username:$password@$host:$port")
        masterName = "sentinel7000"
        role = RedisRole.MASTER
    }
    return Redis.createClient(vertx, redisOptions)
}

class RedisCache(private val redis: Redis) : AuthCache {

    override suspend fun append(token: String, expire: Int?, data: String) {
        val redisApi = redis.createApi()
        redisApi.setAwait(listOf(token, data))
        if (expire != null) redisApi.expireAwait(token, "$expire")
    }

    override suspend fun getData(token: String): String {
        val redisApi = redis.createApi()
        return redisApi.getAwait(token)?.toString() ?: ""
    }

    override suspend fun setData(token: String, data: String): String {
        val redisApi = redis.createApi()
        redisApi.setAwait(listOf(token, data))
        return data
    }

    override suspend fun validate(token: String, expire: Int?): Boolean {
        val redisApi = redis.createApi()
        val exist = redisApi.existsAwait(listOf(token))?.toInteger() ?: return false
        if (exist == 0) return false
        if (expire != null) redisApi.expireAwait(token, "$expire")
        return true
    }

    override suspend fun invalidate(token: String): Boolean {
        val redisApi = redis.createApi()
        redisApi.delAwait(listOf(token))
        return true
    }
}

suspend fun Redis.createApi(): RedisAPI = RedisAPI.api(this)
