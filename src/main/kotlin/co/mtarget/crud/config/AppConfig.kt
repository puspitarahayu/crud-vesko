package co.mtarget.crud.config

import io.vertx.core.json.JsonObject
import io.vertx.kotlin.core.json.jsonObjectOf

data class ApplicationConfig(
    val name: String,
    val host: String,
    val port: Int,
    val rootPath: String = "",
    val rootPathInController: Boolean = true,
    val captchaSecret: String = "",
    val origins: HashSet<String> = hashSetOf(),
    val clusterhost: String = "localhost",
    val clusterConfig: Any? = null
)

data class SwaggerConfig(
    val title: String = "Simple Crud Vesko",
    val description: String = "",
    val servers: List<String> = listOf("http://localhost"),
    val version: String = "v1"
)

fun getApplicationConfig(config: JsonObject): ApplicationConfig {
    return config.getJsonObject("application", jsonObjectOf()).mapTo(ApplicationConfig::class.java)
}

fun getSwaggerConfig(config: JsonObject): SwaggerConfig {
    return config.getJsonObject("swagger", jsonObjectOf()).mapTo(SwaggerConfig::class.java)
}







