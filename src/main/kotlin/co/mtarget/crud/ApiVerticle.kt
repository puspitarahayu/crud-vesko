package co.mtarget.crud

import co.mtarget.crud.config.*
import co.mtarget.crud.controller.*
import co.mtarget.crud.service.*
import co.mtarget.vesko.ApiVerticle
import co.mtarget.vesko.web.ApiSecurity
import co.mtarget.vesko.web.PathConfig
import co.mtarget.vesko.web.TagOption
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Info
import io.swagger.v3.oas.models.servers.Server
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import org.koin.core.KoinComponent
import org.koin.core.get
import org.koin.dsl.module

class ApiVerticle : KoinComponent, ApiVerticle() {
    override val serviceName: String = "crud-service"

    override fun controllers() = listOf(
        get<CrudController>()
    )

    override suspend fun init() {
        initJson()
        enableDependenyInjection {
            load(
                enableJwt(vertx, config),
                enableFileSystem(vertx),
                enableMongodb(config),
                serviceModule(),
                verticleModule
            )
        }
        get<DataService>()
    }


    override fun pathConfig(): PathConfig {
        val applicationConfig = getApplicationConfig(config)
        return PathConfig(applicationConfig.rootPath, applicationConfig.rootPathInController)
    }

    override fun openApi(): OpenAPI {
        val swaggerConfig = getSwaggerConfig(config)
        val openAPI = OpenAPI()
        openAPI.info = Info().apply {
            title = swaggerConfig.title
            version = swaggerConfig.version
            description = swaggerConfig.description
        }
        openAPI.servers = swaggerConfig.servers.map { Server().apply { url = it } }
        return openAPI
    }

    override fun apiSecurity(): ApiSecurity? {
        return ApiSecurity.JWT
    }

    override fun tagOptions(): Set<TagOption> {
        return setOf()
    }
}

val verticleModule = module {
    single { CrudController() }
}

fun serviceModule() = module {
    single { DataService() }

}