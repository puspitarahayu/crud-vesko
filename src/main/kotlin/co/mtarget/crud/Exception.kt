package co.mtarget.crud
import co.mtarget.vesko.ValidationException

class NamaAlreadyRegistered : ValidationException(121, "nama already exist", null)
class DataKosong : ValidationException(121, "Data Kosong", null)